<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_jaune_jonquille">
<article class="item_detail">


<div class="section_normal">
<div class="item">
	<div class="contents_img clearfix">
		<div class="">
			<!--<div><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/img.jpg" alt=""></div>-->
			<h2 class="head_b">スケジュール</h2>
			</div>
	</div>
</div>
<!-- end .section_normal --></div>


<!-- ■開催日（学習塾） -->
<div class="info_table_juku clearfix bg_color_1">
	<div class="it_head eq_height05">
		学習塾
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 16
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>


<!-- ■開催日（育志塾） -->
<div class="info_table_juku clearfix bg_color_2">
	<div class="it_head eq_height05">
		育志塾
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 49
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>


<!-- ■開催日（就活志塾） -->
<div class="info_table_juku clearfix bg_color_3">
	<div class="it_head eq_height05">
		就活志塾
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 62
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>


<!-- ■開催日（現代寺子屋） -->
<div class="info_table_juku clearfix bg_color_4">
	<div class="it_head eq_height05">
		現代寺子屋
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 71
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>


<!-- ■開催日（母親塾） -->
<div class="info_table_juku clearfix bg_color_5">
	<div class="it_head eq_height05">
		母親塾
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 81
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>


<!-- ■開催日（マザーカレッジ 大阪サテライト校） -->
<div class="info_table_juku clearfix bg_color_6">
	<div class="it_head eq_height05">
		マザーカレッジ<br />
		大阪サテライト校
<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 92
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<div id="form"></div>

<!-- ■フォーム -->

<h2 class="head_b mt50">各塾・マザーカレッジお申込みフォーム</h2>
＊ は入力必須です。
<?php
if (have_posts()) : 
	while (have_posts()) :
		the_post();
?>
<?php the_content(); ?>
<?php
	endwhile;
endif;
?>



<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

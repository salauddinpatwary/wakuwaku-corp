<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_jaune_jonquille">
<article class="item_detail">


<h2 class="head_b">What's New</h2>

<?php
if (have_posts()) : 
	while (have_posts()) :
		the_post();
		$date = sprintf( '<time class="entry-date" datetime="%3$s">%4$s</time>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);
?>
<div class="info_table_juku clearfix">
	<div class="it_head eq_height05">
		<span class="meta_date"><?php echo $date; ?></span>
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<div id="post-<?php echo $post01->ID; ?>" class="data_column">
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</div>
	<!-- end .it_data --></div>
<!-- end .info_table_juku --></div>
<?php
	endwhile;
endif;

wak_pagination();
?>


<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

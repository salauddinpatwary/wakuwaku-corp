<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #navigation --></div>

<div id="main_content">
<div id="docs" class="inner bg_jaune_jonquille">
<article class="item_detail">


<h2 class="head_b">What's New</h2>

<?php
if (have_posts()) : 
	while (have_posts()) :
		the_post();
		$date = sprintf( '<time class="entry-date" datetime="%3$s">%4$s</time>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);
?>
<span class="meta_date"><?php echo $date; ?></span>
<h3 style=" border-bottom: solid 1px #c99d2e; padding-bottom: 5px;"><?php the_title(); ?></h3>
<?php the_content(); ?>
<?php
	endwhile;
endif;
?>

<div style=" border-top: solid 1px #c99d2e; padding-top: 10px;"><a href="<?php echo home_url(); ?>/category/news/">What's New へ戻る</a></div>
<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

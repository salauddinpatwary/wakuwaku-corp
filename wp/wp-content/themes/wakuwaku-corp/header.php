<!DOCTYPE html>
<!--[if IE 6]>         <html lang="ja" class="ie6"> <![endif]-->
<!--[if IE 7]>         <html lang="ja" class="ie7"> <![endif]-->
<!--[if IE 8]>         <html lang="ja" class="ie8"> <![endif]-->
<!--[if IE 9]>         <html lang="ja" class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="ja"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="format-detection" content="telephone=no">

	<title><?php wp_title('-', true, 'right'); ?><?php bloginfo('name'); ?></title>

	<link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri(); ?>/assets/reset_base.css" />
	<link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri(); ?>/assets/layout02.css" />
	<link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri(); ?>/assets/customize.css" />
	<link rel="stylesheet" media="print" href="<?php echo get_template_directory_uri(); ?>/assets/print.css" />

	<!--[if gte IE 9]><!-->
	<script src="<?php echo get_template_directory_uri(); ?>/assets/jquery-2.1.1.min.js"></script>
	<!--<![endif]-->
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/jquery-1.11.1.min.js"></script>
	<![endif]-->
	<!--[if lte IE 6 ]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/DD_belatedPNG.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/ie6.js"></script>
	<![endif]-->
	<!--[if lte IE 8]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/html5shiv-min.js"></script>
	<![endif]-->

<!-- sidr がうまくうごかない
	<script src="<?php echo get_template_directory_uri(); ?>/assets/jquery.easing.js"></script>
-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/jquery.sidr.light.css" />
	<script src="<?php echo get_template_directory_uri(); ?>/assets/jquery.sidr.min.js"></script>

<?php if(is_page('37')): ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/bxslider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bxslider/jquery.bxslider.css" />
<script>
/* bxslider */
$(document).ready(function(){
	$('.bxslider').bxSlider({
		nextSelector: '.slider-next',
		prevSelector: '.slider-prev',
		nextText: '<img src="/wp/wp-content/themes/wakuwaku-corp/img/sodatsu-ie/nav_next.jpg" alt="">',
		prevText: '<img src="/wp/wp-content/themes/wakuwaku-corp/img/sodatsu-ie/nav_back.jpg" alt="">',
		adaptiveHeight: true,
		mode: 'fade',
		onSlideAfter: function(){
			// do mind-blowing JS stuff here
			location.href = '#contents_top';
		}
	});
});
</script>
<?php endif; ?>


	<script src="<?php echo get_template_directory_uri(); ?>/assets/layout02.js"></script>

<?php wp_head(); ?>
</head>

<body <?php body_class('column_right_navi'); ?>>

<div id="page">

<div id="header">
<div class="inner clearfix">
<div id="mobile_header"><a id="btn_side_menu" href="#side_menu">Side Menu</a></div>
<div class="logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/logo.png" alt=""></a></div>
<div class="header_parts"><img src="<?php echo get_template_directory_uri(); ?>/assets/header_info.png" alt=""></div>
<!-- end #header .inner --></div>
<!-- end #header --></div>


<div id="g_menu">
<div class="g_menu_target inner clearfix">
<ul class="clearfix">
	<li class="cat_apple"><a href="<?php echo home_url(); ?>/">TOP</a></li>
	<li class="cat_banana"><a href="<?php echo home_url(); ?>/itto/">学習塾</a></li>
	<li class="cat_grape"><a href="<?php echo home_url(); ?>/ikushi/">育志塾</a></li>
	<li class=""><a href="<?php echo home_url(); ?>/syukatu/">就活志塾</a></li>
	<li class=""><a href="<?php echo home_url(); ?>/terakoya/">現代寺子屋</a></li>
	<li class=""><a href="<?php echo home_url(); ?>/schedule/">お申し込み</a></li>
	<li class=""><a href="<?php echo home_url(); ?>/profile/">会社紹介</a></li>
</ul>
<!-- end #g_menu .inner --></div>
<div class="g_menu_mobile">
<ul class="clearfix">
	<li class="cat_apple"><a href="<?php echo home_url(); ?>/mama/">母親塾</a></li>
	<li class="cat_apple"><a href="<?php echo home_url(); ?>/mama-osaka/">マザーカレッジ 大阪サテライト校</a></li>
	<li class="cat_apple"><a href="<?php echo home_url(); ?>/sodatsu-ie/">共育住宅『育つ家』</a></li>
	<li class="cat_apple"><a href="<?php echo home_url(); ?>/schedule/">各塾スケジュール</a></li>
	<li class="cat_apple"><a href="#">What's New</a></li>
	<li class="cat_apple"><a href="#">facebook</a></li>
</ul>
<!-- end #g_menu .g_menu_mobile --></div>
<!-- end #g_menu --></div>

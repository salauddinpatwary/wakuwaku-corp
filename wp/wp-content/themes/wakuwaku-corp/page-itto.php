<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_jaune_jonquille">
<article class="item_detail">


<div class="section_normal">
<div class="item">
	<div class="contents_img clearfix">
		<div class="parts_1">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/itto/img.jpg" alt=""></div>
			<div><a href="<?php echo site_url(); ?>/schedule/#form" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/img/itto/img01_off.jpg" alt=""></a></div>
			</div>
		<div class="parts_2"><img src="<?php echo get_template_directory_uri(); ?>/img/itto/img02.jpg" alt=""></div>
		<div class="parts_3">
			<div><!--<img src="<?php echo get_template_directory_uri(); ?>/img/itto/img03.jpg" alt="">--></div>
			<div><a href="http://e-apples.net/" target="_blank" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/img/itto/img03a_off.jpg" alt=""></a></div>
		</div>
	</div>
</div>
<!-- end .section_normal --></div>

<!-- ■開催要項 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height02">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height02">
		<div class="data_column_half_1">
			<p>■対象：小学生・中学生・高校生</p>
			<p>■頻度：週１～３回</p>
			<p>■時間：１回50分、80分、100分</p>
		</div>
		<div class="data_column_half_2">
			<p>■費用：詳細は<a href="http://e-apples.net/" target="_blank">コチラ</a>にてご確認ください</p>
			<p>■場所：大阪高槻校</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■主な内容 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height03">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img01.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height03">
		<div class="data_column">
<p>成績目標達成に向かって成績を学習させるには、成績目標を意識した学習サイクルを維持することが大事です。</p>
<p>個別指導塾アップルズでは、成績目標設定からテスト対策、予習・復習を、成績目標達成という一貫した視点から見渡して取り組みます。だから、一人ひとりに合わせた成績向上のための学習に取り組むことができます。</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■講師紹介 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height04">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img02.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height04">
		<div class="data_column">
			<div class="fl_left mr10"><img src="<?php echo get_template_directory_uri(); ?>/img/itto/img04.jpg" alt=""></div>
<p>個別指導塾アップルズ　大阪高槻校<br />
教室長　伊藤麦<br />
<span class="weak_size weak">※その他、優秀講師陣が愛情を持って対応させて頂いております。</span></p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■開催日 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height05">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img03.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 16 // スケジュールの記事番号
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■参加者の声 -->

<div><img src="<?php echo get_template_directory_uri(); ?>/assets/voice_head.jpg" alt=""></div>

<div class="section_half item_cycle clearfix bg_clip_1">
<?php
	$args = array(
		'posts_per_page' => 20
	,	'category_name' => "voice-itto" // 参加者の声のカテゴリーslug
	);
	$posts = get_posts( $args );
	global $post;
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>

<div class="item" id="post-<?php echo $post->ID; ?>">
	<div class="clip_head"></div>
	<div class="item_inner">
	<!--<h3><?php the_title(); ?></h3>-->
<?php the_content(); ?>
	</div>
</div>

<?php endforeach; endif; ?>
<!-- end .section_half --></div>

<div class="ta_center pt15 pb15">
<a href="<?php echo site_url(); ?>/schedule/#form" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/assets/mousikomi_off.png" alt=""></a>
</div>

<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

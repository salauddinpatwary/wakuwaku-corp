<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_jaune_jonquille">
<article class="item_detail">


<div class="section_normal">
<div class="item">
	<div class="contents_img clearfix">
		<div class="parts_1">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/terakoya/img.jpg" alt=""></div>
			<div><a href="<?php echo site_url(); ?>/schedule/#form" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/img/terakoya/img01_off.jpg" alt=""></a></div>
			</div>
		<div class="parts_2"><img src="<?php echo get_template_directory_uri(); ?>/img/terakoya/img02.jpg" alt=""></div>
	</div>
</div>
<!-- end .section_normal --></div>

<!-- ■開催要項 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height02">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height02">
		<div class="data_column_half_1">
			<p>■対象：社会人</p>
			<p>■頻度：2ヶ月に１回</p>
			<p>■時間：60～90分程度</p>
		</div>
		<div class="data_column_half_2">
			<p>■費用：無料</p>
			<p>■場所：近隣コミュニケーションセンター等</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■主な内容 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height03">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img01.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height03">
		<div class="data_column">
<p>［基本的な流れ］<br />
・講師の解説<br />
・塾生とワークショップ～塾生徒同士のディスカッション<br />
・講師のまとめ
</p>
<p>［ポイント］<br />
・偉人に対して、自分が感じたことを中心に語り合います。<br />
・子どもや大人、対象別にどう話せばいいかも勉強します。
</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■講師紹介 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height04">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img02.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height04">
		<div class="data_column">
			<div class="fl_left mr10"><img src="<?php echo get_template_directory_uri(); ?>/img/terakoya/img04.jpg" alt=""></div>
<p>
進和グループ株式会社わくわくコーポレーション代表取締役<br />
全塾塾長　七つの習慣担当インストラクター<br />
西田 芳明<br />
<br />
進和グループ進和建設工業株式会社　専務取締役<br />
西田泰久<br /><br />
<span class="weak_size weak">※その他随時専門講師を招待し担当していただきます。</span></p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■開催日 -->
<div class="info_table_juku clearfix">
	<div class="it_head eq_height05">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img03.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 71 // スケジュールの記事番号
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_juku --></div>

<!-- ■参加者の声 -->

<div><img src="<?php echo get_template_directory_uri(); ?>/assets/voice_head.jpg" alt=""></div>

<div class="section_half item_cycle clearfix bg_clip_1">
<?php
	$args = array(
		'posts_per_page' => 20
	,	'category_name' => "voice-terakoya" // 参加者の声のカテゴリーslug
	);
	$posts = get_posts( $args );
	global $post;
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>

<div class="item" id="post-<?php echo $post->ID; ?>">
	<div class="clip_head"></div>
	<div class="item_inner">
	<!--<h3><?php the_title(); ?></h3>-->
<?php the_content(); ?>
	</div>
</div>

<?php endforeach; endif; ?>
<!-- end .section_half --></div>

<div class="ta_center pt15 pb15">
<a href="<?php echo site_url(); ?>/schedule/#form" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/assets/mousikomi_off.png" alt=""></a>
</div>

<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

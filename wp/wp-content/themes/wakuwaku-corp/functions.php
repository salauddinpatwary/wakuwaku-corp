<?php

/**
 *
 */
if ( !function_exists('wak_pagination') ) :
function wak_pagination() {
	global $wp_query;
	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav class='item_pagination clearfix'>
			<div class='previous'><?php next_posts_link( '前へ' ); ?></div>
			<div class='next'><?php previous_posts_link( '次へ' ); ?></div>
		</nav>
	<?php endif;
} // wak_pagination
endif;

?>

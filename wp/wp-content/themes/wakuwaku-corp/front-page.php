<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #navigation --></div>

<div id="main_content">
<div id="docs" class="inner bg_jaune_jonquille">
<article class="item_detail ">

<!-- ここから  -->
<div class="section_normal mb10">
<div class="item mb10"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img.png" alt=""></div>
</div>

<div class="contents_img_home">
<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img01.png" alt=""></div>
<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img02.png" alt=""></div>
<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img03.png" alt=""></div>
<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img04.png" alt=""></div>
</div>

<div class="section_normal">
<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img05.png" alt=""></div>
</div>

<div class="contents_img_home01 clearfix">
<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img06.png" alt=""></div>
<div class="col_left"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img07.png" alt=""></div>
<div class="col_right">
	<div><img src="<?php echo get_template_directory_uri(); ?>/img/home/img08.png" alt=""></div>
	<p>永年企業の経営者をしておりますと、企業経営において何が一番大切か、ということが次第に分かるようになります。それは「人」に尽きる。ということで、その人の善し悪しで決まるのです。<br>
私が必要とする人とは、お客さまのために、地域のために、会社のために、そして愛する家族のために、ポジティブな心で失敗を恐れず、楽しく精一杯頑張れる人のこと。<br>
しかし、残念ながらそうではない人も多い。でもそれは本人がダメというより、子どもの時から育った環境が原因であることがほとんどなのです。</p>
<p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/img/home/img09.png" alt=""></p>
<p>
戦後の教育は“受験対策としての教育” ばかりで結果、学校崩壊、地域崩壊、家庭崩壊、等と言われるようになってしまった。<br>
そして、子どもが「社会の役に立ち幸せに暮らす」という子育ての本質を置き去りにしてきてしまったのではないでしょうか？<br>
<br>
私は子どもが世の中の多くの人に役立ち、喜ばれ、そして幸せな人生へと歩む応援をするために、幼少期から成人になる過程において様々な人育ての塾を展開しています。<br>
<br>
本当の幸せは“心の豊かさ”。心の豊かさな“利他の精神”。だから幸せになるために人は成長し続けるのです。 『人育て』それは、私の人生の最終ミッションです。</p>
	</div>
</div>

<div>




<?php
		if($posts01): foreach($posts01 as $post01): setup_postdata($post01);
		$date_str_ = $post01->post_date;
		$date = 
				substr($date_str_, 0, 4)
				. '年'
				. intval(substr($date_str_, 5, 2))
				. '月'
				. intval(substr($date_str_, 8, 2))
				. '日';
/*
post_title
post_date
guid

*/
?>

<div class="info_table_juku clearfix">
	<div class="it_head eq_height05">
		<span class="meta_date"><?php echo $date; ?></span>
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<div id="post-<?php echo $post01->ID; ?>" class="data_column">
<a href="<?php echo $post01->guid; ?>"><?php echo $post01->post_title; ?></a>
</div>
	<!-- end .it_data --></div>
<!-- end .info_table_juku --></div>

<?php endforeach; endif; ?>





</div>

<!-- ここまで  -->


<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

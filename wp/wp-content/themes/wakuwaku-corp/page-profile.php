<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_light_ultramarine_blue">
<article class="item_detail bg_white">


<div class="section_normal">
<div class="item">
	<div class="contents_img clearfix">
		<div class="">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/profile/img.jpg" alt=""></div>
			</div>
		<div class=""><img src="<?php echo get_template_directory_uri(); ?>/img/profile/img01.jpg" alt=""></div>
	</div>
</div>
<!-- end .section_normal --></div>

<!-- ■ -->
<div class="section_normal item_cycle clearfix">
<div class="item">
<div class="notes_info">
<dl>
	<dt>社名</dt>
	<dd>株式会社わくわくコーポレーション</dd>

	<dt>所在地</dt>
	<dd>（本社）〒591-8032大阪府堺市北区百舌鳥梅町1-30-1<br>
   	（大阪高槻校）〒569-1042 大阪府高槻市南平台4丁目3番2号</dd>

	<dt>電話</dt>
	<dd>（本社） 0120-576-788<br>
（大阪高槻校）072-668-2653</dd>

	<dt>資本金</dt>
	<dd>10,000,000円</dd>

	<dt>塾紹介</dt>
	<dd>個別指導塾アップルズ（入試対策）<br />
育志塾（人づくりを目指す）<br />
就活志塾（社会人予備校） <br />
現代寺子屋（歴史から学ぶ社会人育成）<br />
母親塾（お母さんたちの憩いの場）<br />
マザーカレッジ大阪サテライト校（子どもの成長を伸ばす秘訣）
</dd>

</dl>
</div>
</div>
<!-- end section_normal --></div>


<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>

<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_usubenifujiiro">
<article class="item_detail">


<div class="section_normal">
<div class="item">
	<div class="contents_img clearfix">
		<div class="">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/mama-osaka/img.jpg" alt=""></div>
			</div>
		<div class=""><a href="http://www.mothercollege.com/" target="_blank" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/img/mama-osaka/img01_off.jpg" alt=""></a></div>
		<div class="">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/mama-osaka/img02.jpg" alt=""></div>
		</div>
	</div>
</div>
<!-- end .section_normal --></div>

<!-- ■開催要項 -->
<div class="info_table_mama clearfix">
	<div class="it_head eq_height02">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img04.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height02">
		<div class="data_column_half_1">
			<p>■対象：賢い子育てを望まれる<br />
				お母様・お父様</p>
			<p>■頻度：一月に１度</p>
			<p>■時間：2時間</p>
		</div>
		<div class="data_column_half_2">
			<p>■費用：入会金／１２，０００円　　<br />
				受講料／２９，８００円<br />
ベーシックコース全４回分</p>
			<p>■場所：AP大阪梅田茶屋町（<a href="http://www.ap-umedachayamachi.com/info/access.html" target="_blank">アクセス</a>）</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_mama --></div>

<!-- ■主な内容 -->
<div class="info_table_mama clearfix">
	<div class="it_head eq_height03">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img05.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height03">
		<div class="data_column">
<p>［ベーシックコース］<br />
・ペアレンテーションとは？　脳の学習理論<br />
・子どもとのよりよいコミュニケーションの取り方<br />
・子どもの気持ちを引き出し方・やる気の引き出し方<br />
・目標達成できる強い子どもの育て方
</p>
<p>［アドバンスコース］<br />
・自立した子どもを育てる<br />
・子どもとの関わりあい方（ケーススタディ）<br />
・心と身体の使い方（身体を使った能力アップ法）<br />
・マザーのためのセルフマネジメント<br />
・ペアレンテーション科の認定講師を育てる「プロフェッショナルコース」もあります。
</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_mama --></div>

<!-- ■講師紹介 -->
<div class="info_table_mama clearfix">
	<div class="it_head eq_height04 clearfix">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img06.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height04">
		<div class="data_column clearfix">
			<div class="fl_left mr10 pb5"><img src="<?php echo get_template_directory_uri(); ?>/img/mama-osaka/img04.jpg" alt=""></div>
<p>マザーカレッジ主宰　江藤真規<br />
マザーカレッジ認定講師<br /><br />
</p>
		</div>
	<!-- end .it_head --></div>
<!-- end .info_table_mama --></div>

<!-- ■開催日 -->
<div class="info_table_mama clearfix">
	<div class="it_head eq_height05">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/it_head_img07.png" alt="">
	<!-- end .it_head --></div>
	<div class="it_data clearfix eq_height05 data_schedule">
<?php
	$args01 = array(
		'p' => 92 // スケジュールの記事番号
	);
	$posts01 = get_posts( $args01 );
	global $post01;
?>
<?php if($posts01): foreach($posts01 as $post01): setup_postdata($post01); ?>
<div id="post-<?php echo $post01->ID; ?>">
<?php the_content(); ?>
</div>
<?php endforeach; endif; ?>
	<!-- end .it_head --></div>
<!-- end .info_table_mama --></div>

<!-- ■参加者の声 -->

<div><img src="<?php echo get_template_directory_uri(); ?>/assets/voice_head01.jpg" alt=""></div>

<div class="section_half item_cycle clearfix bg_clip_2">
<?php
	$args = array(
		'posts_per_page' => 20
	,	'category_name' => "voice-mama-osaka" // 参加者の声のカテゴリーslug
	);
	$posts = get_posts( $args );
	global $post;
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>

<div class="item" id="post-<?php echo $post->ID; ?>">
	<div class="clip_head"></div>
	<div class="item_inner">
	<!--<h3><?php the_title(); ?></h3>-->
<?php the_content(); ?>
	</div>
</div>

<?php endforeach; endif; ?>
<!-- end .section_half --></div>

<div class="ta_center pt15 pb15">
<a href="<?php echo site_url(); ?>/schedule/#form" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/assets/mousikomi_off.png" alt=""></a>
</div>

<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>




/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    external file
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/**
 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.4
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,e,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);


/* DEVICE_TYPE */
var DEVICE_TYPE = new (function() {
	this.ua			=	window.navigator.userAgent;
	this.IE			=	document.uniqueID ? true : false;
	this.IE8		=	this.ua.indexOf('MSIE 8.') != -1 ? true : false ;
	this.IE9		=	this.ua.indexOf('MSIE 9.') != -1 ? true : false ;
	this.lteIE6		=	typeof window.addEventListener == 'undefined' &&
						typeof document.documentElement.style.maxHeight == 'undefined';
	this.lteIE7		=	typeof window.addEventListener == 'undefined' &&
						typeof document.querySelectorAll == 'undefined';
	this.lteIE8		=	!/Trident\/5.0/i.test(this.ua) &&
						typeof window.addEventListener == 'undefined' &&
						typeof document.getElementsByClassName == 'undefined';
	this.lteIE9		=	this.lteIE8 || this.IE9;

	this.gteIE10	=	this.IE && typeof history.pushState === 'function';
	this.gteIE9		=	this.IE && typeof window.getSelection === 'function';
	this.gteIE8		=	this.gteIE9 || this.IE8;

    this.iPhone 	=	navigator.userAgent.match(/iPhone|iPod/i);
    this.iPad		=	navigator.userAgent.match(/iPad|iPad Simulator/i);
    this.iOS		=	this.iPhone || this.iPad;

    this.AndroidPhone	=	navigator.userAgent.match(/Android.*Mobile/i);
    this.AndroidTablet	=	( navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/Mobile/i) );
    this.Android		=	this.AndroidPhone || this.AndroidTablet;
    this.BreakPoint01 = 360;
    this.BreakPoint02 = 768;
    this.bp = function(_w) {
		if ( this.BreakPoint01>=_w  ) { // 360以下
			return 0;
		} else if ( this.BreakPoint02>=_w && _w>this.BreakPoint01 ) { // 361以上、600以下
			return 1;
		} else if ( _w>this.BreakPoint02 ) { // 600以上
			return 2;
		}
	};
})();
DEVICE_TYPE.BreakPoint01=360;
DEVICE_TYPE.BreakPoint02=768;

/*!--------------------------------------------------------------------------*
 *  
 *  jquery.heightLine.js
 *  
 *  MIT-style license. 
 *  
 *  2013 Kazuma Nishihata 
 *  http://www.to-r.net
 *  
 *--------------------------------------------------------------------------*/
;(function($){
	$.fn.heightLine = function(){
		var target = this,fontSizeChangeTimer,windowResizeId= Math.random();
		var heightLineObj = {
			op : {
				"maxWidth" : 10000,
				"minWidth" : 0,
				"fontSizeCheck" : false
			},
			setOption : function(op){
				this.op = $.extend(this.op,op);
			},
			destroy : function(){
				target.css("height","");
			},
			create : function(op){
				var self = this,
					maxHeight = 0,
					windowWidth = $(window).width();
				self.setOption(op);
				if( windowWidth<=self.op.maxWidth && windowWidth>=self.op.minWidth ){
					target.each(function(){
						if($(this).outerHeight()>maxHeight){
							maxHeight = $(this).outerHeight();
						}
					}).each(function(){
						var height = maxHeight
								   - parseInt($(this).css("padding-top"))
								   - parseInt($(this).css("padding-bottom"));
						$(this).height(height);
					});
				}
			},
			refresh : function(op){
				this.destroy();
				this.create(op);
			},
			removeEvent :function(){
				$(window).off("resize."+windowResizeId);
				target.off("destroy refresh");
				clearInterval(fontSizeChangeTimer);
			}
		}
		if(typeof arguments[0] === "string" && arguments[0] === "destroy"){
			target.trigger("destroy");
		}else if(typeof arguments[0] === "string" && arguments[0] === "refresh"){
			target.trigger("refresh");
		}else{
			heightLineObj["create"](arguments[0]);
			
			$(window).on("resize."+windowResizeId,function(){
				heightLineObj["refresh"]();
			});

			target.on("destroy",function(){
				heightLineObj["removeEvent"]();
				heightLineObj["destroy"]();
			}).on("refresh",function(){
				heightLineObj["refresh"]();
			});

			if(heightLineObj.op.fontSizeCheck){
				
				if($("#fontSizeChange").length<=0){
					var fontSizeChange = $("<span id='fontSizeChange'></span>").css({
						width:0,
						height:"1em",
						position:"absolute",
						left:0,
						top:0
					}).appendTo("body");
				}
				var defaultFontSize = $("#fontSizeChange").height();
				fontSizeChangeTimer = setInterval(function(){
					if(defaultFontSize != $("#fontSizeChange").height()){
						heightLineObj["refresh"]();
					}
				},100);
			}
		}
		return target;
	}
})(jQuery);




/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    org
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

// element height
$(function(){
//	$(".section_thumb .item").heightLine();
	$(".eq_height .item").heightLine();
	$(".eq_height01 .item").heightLine();
	$(".eq_height02").heightLine({
		minWidth: 601
	});
	$(".eq_height03").heightLine({
		minWidth: 601
	});
	$(".eq_height04").heightLine({
		minWidth: 601
	});
	$(".eq_height05").heightLine({
		minWidth: 601
	});
});


// Odd Even Set
$(function(){
	$('ul').each(function(){
		$('>li:first', this).addClass('first');
		$('>li:odd', this).addClass('odd');
		$('>li:even', this).addClass('even');
		$('>li:last', this).addClass('last');
		$('>li:nth-child(3n)', this).addClass('cycle03');
		$('>li:nth-child(4n)', this).addClass('cycle04');
	});
	$('dl').each(function(){
		$('>dt:first', this).addClass('first');
		$('>dt:odd', this).addClass('odd');
		$('>dt:even', this).addClass('even');
		$('>dt:last', this).addClass('last');
		$('>dd:first', this).addClass('first');
		$('>dd:odd', this).addClass('odd');
		$('>dd:even', this).addClass('even');
		$('>dd:last', this).addClass('last');
	});
	$('table').each(function(){
		$('>tr:first', this).addClass('first');
		$('>tr:odd', this).addClass('odd');
		$('>tr:even', this).addClass('even');
		$('>tr:last', this).addClass('last');
	});
	$('.item_cycle').each(function(){
		$('.item:first', this).addClass('item_first');
		$('.item:eq(1)', this).addClass('item_second');
		$('.item:eq(2)', this).addClass('item_third');
		$('.item:eq(3)', this).addClass('item_fourth');
		$('.item:odd', this).addClass('item_odd');
		$('.item:even', this).addClass('item_even');
		$('.item:last', this).addClass('item_last');
		$('.item:nth-child(3n)', this).addClass('cycle03');
		$('.item:nth-child(4n)', this).addClass('cycle04');
	});
	$('.unit_cycle').each(function(){
		$('.unit:first', this).addClass('unit_first');
		$('.unit:eq(1)', this).addClass('unit_second');
		$('.unit:eq(2)', this).addClass('unit_third');
		$('.unit:eq(3)', this).addClass('unit_fourth');
		$('.unit:odd', this).addClass('unit_odd');
		$('.unit:even', this).addClass('unit_even');
		$('.unit:last', this).addClass('unit_last');
		$('.unit:nth-child(3n)', this).addClass('cycle03');
		$('.unit:nth-child(4n)', this).addClass('cycle04');
	});
});



$(function() {

	// サイドメニュー　開閉
	$('#btn_side_menu').sidr({
		name: 'side_menu'
		, source: '#g_menu'
		, side: 'right'
	});

	// g_menu にクラス付け
	$g_menu = $('#g_menu > div.g_menu_target > ul');
	$g_menu.addClass('step01_wrap');
	$('> li', $g_menu).addClass('step01').each(function(i){
		$(this).addClass('step01_'+i);
		$('> ul', this).addClass('step02_wrap').each(function(){
			$('> li', this).addClass('step02').each(function(j){
				$(this).addClass('step02_'+j);
			});
		});
	}); // #g_menu

	$('> li > a', $g_menu).each(function(){
		var href_ = $(this).attr('href');
		var loc_href_ = window.location.href; // このページのURL
		if ( href_ == loc_href_ ) {
			$(this).addClass('current_page');
		}
	});


	// side_menu にクラス付け
	$side_menu = $('.side_menu > ul');
	$side_menu.addClass('step01_wrap');
	$('> li', $side_menu).addClass('step01').each(function(i){
		$(this).addClass('step01_'+i);
		$('> ul', this).addClass('step02_wrap').each(function(){
			$('> li', this).addClass('step02').each(function(j){
				$(this).addClass('step02_'+j);
			});
		});
	}); // .side_menu
	$('a', $side_menu).wrapInner('<span class="a_inner"></span>');


	// 
}); // function


// scrollTo
$(function() {
	var $pagetop = $('#go_pagetop a');
	var $page_index = $('.page_index a');
	
	$pagetop.click(scrollPage);
	$page_index.click(scrollPage);
	
	function scrollPage(e){
		e.preventDefault(e);
		var whereTo = $(this).attr("href");
		var targetLink = whereTo.substring(1); // hash
		$.scrollTo($("#"+targetLink),600,{axis:'y',easing:'easeOutCubic'});
	}
});

/* ページの先頭へ戻る
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
$(function () {
	var $obj = $("#go_pagetop");
	if ( !DEVICE_TYPE.lteIE7 ) {
		$obj.hide();
		$(window).scroll(function () {
			var _w = $(window).width();
//			if ( DEVICE_TYPE.bp(_w)==2 ) { // 600以上
			if ( 1 ) { // 条件なし
	
				var scroll_top = $(this).scrollTop();
				var page_height = $('#page').height();
				var window_height = $(window).height();
				var a = page_height-scroll_top; 
				var b = (page_height-scroll_top)-window_height;
				var c = 78 -b;
				if ( b<78 ) {
					$obj.css({'bottom':c});
				} else {
					$obj.css({'bottom':0});
				}
				if ( scroll_top> 300) {
					$obj.fadeIn();
				} else {
					$obj.fadeOut();
				}
			} else { // 600以上
				$obj.show();
				$obj.css({'bottom': 'auto'});
			}
		}); // scroll
	} else { // lteIE7
	}
});

// copyright year
$(function(){
	var now = new Date();
	var year = now.getFullYear();
	$('.copyright_year').html( year );
});

// 開閉トグル
$(function(){
	$(".toggle_button").on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass('toggle_active')
	}); // toggle_button click
});


// 開閉トグル 一括
$(function(){
	$(".toggle_button_bulk").on("click", function() {
		$('.toggle_content_bulk').slideToggle();
		$('.toggle_content_bulk').toggleClass('toggle_active')
	}); // toggle_button_bulk click
});

// 画像ロールオーバー
// Rollover
$(function(){
	$('a.rollover_img img').hover(
		function(){ $(this).attr('src', $(this).attr('src').replace('_off', '_on')); },
		function(){
			if (!$(this).hasClass('current_page')) {
				$(this).attr('src', $(this).attr('src').replace('_on', '_off'));
			}
		}
	); // hover
});

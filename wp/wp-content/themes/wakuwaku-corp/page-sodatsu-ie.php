<?php get_header(); ?>

<div id="contents" class="clearfix">

<div id="sub_content">
<div class="inner">
<?php get_sidebar(); ?>
</div>
<!-- end #sub_content --></div>

<div id="main_content">
<div id="docs" class="inner bg_lightblue">
<article class="item_detail sodatsu_ie_wrap">


<div class="section_normal">
<div class="item">
	<div class="contents_img clearfix">
		<div class="">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img.jpg" alt=""></div>
			<div class="ta_center"><strong>HP上でダイジェストをご覧いただけます。<br />
			また<a href="<?php echo home_url(); ?>/schedule/#form">冊子をプレゼント</a>していますのでぜひお申込みください</strong></div>
			</div>
	</div>
</div>
<!-- end .section_normal --></div>


<div id="contents_top"></div>

<div class="nav_wrap clearfix">
	<div class="nav_left slider-prev" id=""></div>
	<div class="nav_right slider-next" id=""></div>
</div>


<ul class="bxslider">
<!-- ■1 -->
<li><img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img01.jpg" alt=""></li>

<!-- ■2 -->
<li>  
<img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img02.jpg" alt="">
<div class="ie_text">
子ども達に関する社会現象が話題になって久しい現在。<br />
世の中の不景気や就職難、将来への漠然とした不安といった社会背景はあるにせよ、少子化が進むことによる人口減少や超高齢化社会に向かう日本では、子どもや若者の教育が重要な課題となっています。<br />
子ども達が将来、豊かで幸せな人生をおくるためにも、そしてやる気をもって充実した人生にするためにも「子どもの教育」を根本から見直す時期に来ているのではないでしょうか。
</div>
</li>

<!-- ■3 -->
<li>  
<div class="ie_text">
目指すべきは「子ども達が興味をもって自分から学びたいと思い、自信を持って育っていく」ことであり、「教が育を超えてはならない」という教育方法を踏まえた子育てです。<br />
<br />
そもそも「教育」とは、内にある潜在的なものを外に引っ張り出すという姿勢が大切であり、教えるという行為がその潜在的に持っている「学びたい」とか「知りたい」という欲求を阻害してはなりません。<br />
すべてを教えて、それを習得させ、暗記させて、そのとおりにやらせるという現状の学校や塾の教育方法が、突発的なアクシデントに対応できず、やる気もなく、他人まかせな大人を作っています。<br />
人生にはさまざまな出来事があり、乗り越えなければならない壁も多くあります。その壁に立ち向かい自分を信じ創意工夫で乗り越えていってこそ豊かで幸せな人生が待っています。<br />
そうなるためには自信を持って前向きに努力を続けるという経験や、乗り越えたときの感動を子どものころから体感させることが大切です。
</div>
<img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img03.jpg" alt="">
<div class="ie_text">
現在、さまざまな分野で分業化が進んでいますから、教育は学校とか塾に任せておけばいいと考えている親が多いようです。<br />
しかし知識やノウハウは学べても、知恵やしつけや対人関係はこれらの場所では教えてくれません。ましてや自信を持って社会の中で生きていく方法を教えてくれるわけではありません。<br />
これら豊かで幸せな人生をおくるために必要な知恵や対人スキルは、小さいときから各家庭で教えていくことが大切です。<br />
この「親学」とも言うべき、豊かで幸せな人生をおくるための教育や子育てを、スムーズにそして効果的に実践する環境のひとつが住まいという器（ハード）です。<br />
<br />
モノがあふれ、便利で不自由のない暮らしができる現在では、我慢するとか工夫しなげればならない状況は少なくなりました。<br />
現状では子ども達が欲しいと思う前に祖父母や両親がおもちゃを買い与えたり必要な道具や環境を整えたりします。<br />
その結果、子ども達は必要なモノや環境はあって当たり前と育ち、たとえ欲しいものがあったとしても買えばいいという単純な発想しかできなくなってしまいます。<br />
<br />
どこかで売っているモノや環境ならお金を払って買えばすみますが、お金では買えない感動や生きがいや人とのつながりが必要になったときに、どのように考え行動していいのかわからず、悩んでしまうかあきらめてしまうのではないでしょうか。<br />
さらに「欲しい」と思う前に、「知りたい」と思う前に、そして「学びたい」と思う前に親が様々な知識や物を与えることで、子ども達はインプットが多くなります。<br />
自分自身が必要だと思う前にこれらのインプットが多くなってしまうと、アウトプットが出てこなくなると言い、全てに「受身」な人に育ってしまうと言われています。<br />
その結果がニートになったり引きこもりになったり、いじめや犯罪につながっているように思います。<br />
子ども達の自発的な成長力を養うためには、あまり早く教育をはじめてはいけないという意見もあります。<br />
「見たい」「聞きたい」「知りたい」という欲求の目覚めに応じてモノや知識を与えていくべきだといいます。<br />
つまり興味や関心、欲求、欲望を呼び覚ますための教育がまずあり、それらの欲求に応じて適切な教育を行うことが大切だということです。<br />
</div>
<img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img04.jpg" alt="">
</li>

<!-- ■4 -->
<li>  
<div class="ie_text">
名城大学の講師であり社員教育や子どもの教育に詳しい哲学者「芳村思風」氏によれば、子どもの教育には育て方が重要だと言っています。<br />
育て方を間違えると人間をアウトプットのない受身な大人にしてしまうし、うっかりすると人間を人の顔をした獣にしてしまう恐れもあるということを考える必要があると言っています。<br />
親や先生が子どもに問題を与え考えさせるというシステムは、子どもの自発性を無くしていきます。<br />
問題とは「問う」という心であり子どもの中から出てくるように仕向けなければなりません。その「問う」ということの中に「欲求」や「欲望」が含まれているのです。<br />
実は、この自分の中から湧いてくる問題に対して、どういうふうに考えたらいいのかという知恵を養うために、自由奔放に遊ばせることが大切だと言われています。<br />
子ども達が遊びの中で何らかの問題にぶつかり、どうしたらいいのかという創意工夫をしていく行為が知恵をつくるのです。<br />
また、家庭において親が教えなければならないことは、知識ではなく「価値」だと言われています。価値とは「意味」であり「値打ち」であり「すばらしさ」です。何かをやろうとしたとき「どこに面白さがあるのか」、「どこがすばらしいのか」、「そんな能力を持てばそんなことができるのか」という実感を経験させることが大切です。<br />
この「価値」を感じた子どもは放っておいても自分で必要なものを求め始めるといいます。<br />
<br />
哲学者「芳村思風」氏は、子どもの年齢によってどのような資質・能力を養わなければならないかを、具体的にあげています。（年齢別具体例を参照）<br />
</div>
<img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img05.jpg" alt="">
<img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img06.jpg" alt="">
<img src="<?php echo get_template_directory_uri(); ?>/img/sodatsu-ie/img07.jpg" alt="">
<div class="ie_text">
さらに、教育とは子どもの人生を考え与えておかなければならない「知恵」を与えることであり、両親が生きてきた成果や結果、つかんだものを子どもに与えることだと言っています。また、教育において一番大事なものは言葉ではなく雰囲気だとも言っています。<br />
雰囲気とは目つきや態度、表情がかもし出すもので感性です。言葉という理性はうそが言えますがこの感性はうそが言えません。<br />
子どもの感受性は往々にしてうそを見抜きます。いくら良いことを言葉で言っても態度や表情による雰囲気でうそを見抜くのです。<br />
そこで最後に最も大切なことこそ、父親とか母親とは一体何なのかを常に自分で意識しながら生きるという姿勢だと言われています。子どもは親の背中を見て育つといわれますが、まさに親の生きる姿勢が大きな教育効果を持つということです。<br />
住まいというものは疲れを癒す休息の場でもあり、くつろぎたい場所でもありますが、だらしなく暮らしていては子どもに悪い影響を与えてしまうことにもつながります。休息したりくつろいだりすることと、だらしなくすることをしっかりと区別し、メリハリをつけて生活することが望まれます。<br />
さらに両親の生き様、どういう気持ちや態度で人生を過ごしているかを伝えることが、子どもの教育に大きな影響を与え、豊かで幸せな人生がおくれる人を育てていくのだと言われています。<br />
<br />
現在の住まいや住環境をみてみると、子育てや教育ということをまったく考えていないものが多く存在します。住まい造りにおいて性能や立地は大切な項目ですが、住まいは豊かで幸せな人生がおくれる人を育てる環境だと考えたとき、子育てや教育のプロが住まいをプロデュースするべきなのではないかと考えました。<br />
すべての始まりは、現在の若者たちが抱える問題や、将来の社会問題への解決策のひとつとして、子育てや教育というものを考え直すことが大切なのではないかという発想です。今こそ教育を考え直し、子育てを見直す「教育論」が重要であり、教育のプロである教育関係者がプロデュースする住まいや住環境が、将来の豊かで幸せな社会を築いていくと確信しています。<br />
</div>
</li>

</ul>

<div class="nav_wrap clearfix">
	<div class="nav_left slider-prev" id=""></div>
	<div class="nav_right slider-next" id=""></div>
</div>

<div class="ta_center pt15 pb15">
<a href="<?php echo site_url(); ?>/schedule/#form" class="rollover_img"><img src="<?php echo get_template_directory_uri(); ?>/assets/mousikomi_off.png" alt=""></a>
</div>

<!-- end .item_detail --></article>

<!-- end .inner --></div>
<!-- end #main_content --></div>

<!-- end #contents --></div>

<?php get_footer(); ?>
